const app = Vue.createApp({
    data(){
        return{
            data: {
                "URL": "https://www.spiegel.de/auto/moselbruecke-a61-bei-koblenz-schaeden-an-zweithoechster-autobahnbruecke-deutschlands-a-43edc175-e081-4f7d-84d1-d0ac1953f0fa",
                "Datum": "12.02.2023",
                "Headline": "»Rissbildungen an den Schweißnähten im Brückenkörper«",
                "Subline": "Schäden an zweithöchster Autobahnbrücke",
                "Teaser": "Die Diagnose klingt übel: Wie viele Tausend deutsche Brücken ist auch die Moselbrücke bei Winningen marode und muss dringend saniert werden. Erste Maßnahme: strenge Regeln für die Überfahrt.",
                "Text": "Der Zahn der Zeit nagt, die tägliche Belastung hinterlässt ihre Spuren: Deutschlands zweithöchste Autobahnbrücke weist Schäden auf, und zwar so gravierende, dass sie nur noch mit reduzierter Geschwindigkeit befahren werden kann. Auf den rechten Spuren der Winninger Brücke nahe Koblenz gilt inzwischen Tempo 60 und auf ihren linken, nur für Autos zugelassenen Fahrstreifen Tempo 80. Das teilte die Autobahngesellschaft des Bundes mit. Lastwagen müssen demnach auch bei Staus einen Abstand von mindestens 50 Metern einhalten, Großraum- und Schwertransporte sind vorerst verboten. Grund sind laut Autobahngesellschaft »Rissbildungen an den Schweißnähten im Brückenkörper, deren Ursache sich auf eine Ermüdung von geschweißten Stahlverbindungen zurückführen lässt«. Die laufenden Untersuchungen an der 1972 gebauten und 136 Meter hohen Brücke seien umfangreich. Derzeit begutachten Experten von einem sogenannten Brückenuntersichtgerät aus das 935 Meter lange Bauwerk. »Im Frühjahr 2023 wird über weitere Schritte informiert«, erklärt die Autobahngesellschaft. Die starke Zunahme des Lkw-Verkehrs und die Erhöhung der Achslasten hätten die Belastungen der Brücke der Autobahn 61 erhöht. Das könne zu einer »vorzeitigen Materialermüdung und damit zu einer Verkürzung der Nutzungsdauer« führen. Das gleiche Problem gibt es bei etlichen anderen Straßenbrücken der Sechziger- und Siebzigerjahre in Rheinland-Pfalz. Deutschlands höchste Autobahnbrücke ist mit 185 Metern die Kochertalbrücke in Baden-Württemberg. Die Hochmoselbrücke beim rheinland-pfälzischen Zeltingen-Rachtig übertrumpft zwar mit 158 Metern ebenfalls die Winninger Brücke, ist aber Teil einer Bundesstraße (B 50). Die Brücke über die Mosel ist nur eine von vielen, die reparaturbedürftig sind – oder gar gesperrt und komplett ersetzt werden müssen. Das ist das Ergebnis einer internen Untersuchung der bundeseigenen Autobahngesellschaft an"
                }
        }
    }
    
});

app.mount("#container");